CC = gcc

CFLAGS = -c -Wall

LIBS = -lm

all: zadanie

zadanie: main.o fun.o
	$(CC) -Wall -o zadanie main.o fun.o $(LIBS)
	
main.o: main.c fun.h
	$(CC) $(CFLAGS) main.c 
	
fun.o: fun.c
	$(CC) $(CFLAGS) fun.c
	
clean:
	rm -rf *o zadanie
	
	
