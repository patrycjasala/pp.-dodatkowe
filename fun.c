//
//  fun.c
//  dodatkowe
//
//  Created by Patrycja Sala on 22/06/2020.
//  Copyright © 2020 Patrycja Sala. All rights reserved.
//

#include <stdio.h>
#include <math.h>
#include "fun.h"

void zmiana(double *tab, struct tablica *t)
{
    int i = 0;
    for (i = 0; i < 10; i++)
        *(tab+i) += sqrt(i);
    
    int x;
    printf("\n\nwpisz liczbe przez ktora chcesz pomnozyc liczby z tablicy:\n");
    scanf("%d", &x);
    
    for (i = 0; i < 10; i++)
        *(t->tab + i) = *(tab+i) * x;
    
    return;
}


