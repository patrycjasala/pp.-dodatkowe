//
//  main.c
//  dodatkowe
//
//  Created by Patrycja Sala on 22/06/2020.
//  Copyright © 2020 Patrycja Sala. All rights reserved.
//
//Proszę napisać program, który będzie miał załączony moduł i będzie się kompilował przez Makefile.
//Program bazowy ma wczytać z pliku 10 liczb typu double do tablicy statycznej.
//Moduł ma obsłużyć funkcję/procedurę, która jako parametry będzie mieć tablicę statyczną oraz zmienną strukturalną.
//Funkcja/procedura zaktualizuje kolejne elementy tablicy statycznej używając wskaźnika do dostania się do poszczególnych elementów, przypisując im wartości powiększone o pierwiastek z indeksu danej pozycji w tablicy.
//Tak powstałe wartości mają zostać przemnożone przez liczbę podaną przez użytkownika i przypisane do tablicy będącej elementem składowej przekazanej struktury.
//
//Główny program wypisze wartości tablicy przed zmianą, po zmianie oraz wartości z tablicy w strukturze.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "fun.h"

void wypisanietablicy(double tab[]);

int main()
{
    double tab[10];
    struct tablica tab2;
    
    FILE *plik;
    plik = fopen("liczby.txt","r");
    
    if(plik == NULL)
    {
        printf("\ncos poszlo nie tak\n\n");
        exit(1);
    }
    
    int i = 0;
    for(i = 0; i < 10; i++) //wczytanie liczb z pliku do tablicy
        fscanf(plik, "%lf", &tab[i]);
    
    printf("tablica z liczbami wczytanymi z pliku:\n");
    wypisanietablicy(tab);
    
    zmiana(tab, &tab2);
    
    printf("\ntablica po powiekszeniu wartosci o pierwiastek z indeksu danej pozycji w tablicy:\n");
    wypisanietablicy(tab);
    
    printf("\n\nwartosci zapisane w strukturze (pomnozone przez podana liczbe):\n");
    wypisanietablicy(tab2.tab);
    
    printf("\n\n");
    
    fclose(plik);
    
    getchar();getchar();
    
    return 0;
}

void wypisanietablicy(double tab[])
{
    int i = 0;
    for(i = 0; i < 10; i++)
        printf("%.4lf ", tab[i]);
    return ;
}
