//
//  fun.h
//  dodatkowe
//
//  Created by Patrycja Sala on 22/06/2020.
//  Copyright © 2020 Patrycja Sala. All rights reserved.
//

#ifndef FUN_H
#define FUN_H

struct tablica
{
    double tab[10];
};

void zmiana(double *tab, struct tablica *t);

#endif /* fun_h */
